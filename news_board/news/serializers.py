from rest_framework import serializers
from rest_framework.response import Response

from news.models import Post, Comment


class PostSerializer(serializers.ModelSerializer):
    amount_of_upvotes = serializers.IntegerField(read_only=True)

    class Meta:
        model = Post
        fields = "__all__"


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = "__all__"


class UpvodesSerializer(serializers.ModelSerializer):

    post = serializers.SlugRelatedField(
        queryset=Post.objects.all(), slug_field="id", required=True, write_only=True
    )

    class Meta:
        model = Post
        fields = ("amount_of_upvotes", "post")

    def create(self, validated_data):
        post = validated_data.get("post")
        post.amount_of_upvotes += 1
        post.save()
        return post
