from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response


from news.models import Post, Comment
from news.serializers import PostSerializer, CommentSerializer, UpvodesSerializer


class PostCreateView(generics.CreateAPIView):
    serializer_class = PostSerializer


class PostListView(generics.ListAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()


class PostDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()


class UpvodeView(APIView):
    serializer_class = UpvodesSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()

            return Response(serializer.data)
        return Response(serializer.errors)

    def put(self, request):
        Post.objects.all().update(amount_of_upvotes=0)
        return Response(status=200)


class CommentCreateView(generics.CreateAPIView):
    serializer_class = CommentSerializer


class CommentListView(generics.ListAPIView):
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()


class CommentDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()
