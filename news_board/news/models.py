from django.db import models

# Create your models here.


class Post(models.Model):

    title = models.CharField(max_length=255)
    link = models.URLField(max_length=255)
    creation_date = models.DateTimeField(auto_now_add=True)
    amount_of_upvotes = models.IntegerField(default=0)
    author_name = models.CharField(max_length=120)


class Comment(models.Model):

    author_name = models.CharField(max_length=120)
    content = models.TextField()
    creation_date = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
