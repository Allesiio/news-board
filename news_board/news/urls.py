from django.urls import path

from . import views


app_name = "news"

urlpatterns = [
    path("post/create", views.PostCreateView.as_view()),
    path("post/all", views.PostListView.as_view()),
    path("post/detail/<int:pk>", views.PostDetailView.as_view()),
    path("comment/create", views.CommentCreateView.as_view()),
    path("comment/all", views.CommentListView.as_view()),
    path("comment/detail/<int:pk>", views.CommentDetailView.as_view()),
    path("post/upvode", views.UpvodeView.as_view()),
]
