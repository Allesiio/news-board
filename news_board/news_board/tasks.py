from celery.task.schedules import crontab
from celery.decorators import periodic_task

from news.models import Post


@periodic_task(
    run_every=(crontab(hour=10, day_of_week="*")),
    name="reset_upvotes",
    ignore_result=True,
)
def reset_upvotes():
    Post.objects.all().update(amount_of_upvotes=0)
