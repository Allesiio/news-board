# NEWS BOARD #

## Download the repository with the project ##

* Login in https://bitbucket.org/
* Follow the link https://bitbucket.org/Allesiio/news-board/src/master/
* Push 'Clone' - for download repository with project

## Create virtual enviroment ##

* go to folder with project in command line, and execute command 'source virtualenv "name of virtual enviroment"'
* activate virtual enviroment: in command line execute command: 'source name of virtual enviroment/bin/activate'
* go to install all requirements, which are described in 'requirements.txt' file
* in command line execute command 'source env_variables.sh'

## Installation all requirements ##

* Go to folder with project, and open file 'requirements.txt'
* In command line execute command 'pip install "name of the package to be installed"'

## Migration database and runserver ##

* go to directory with 'manage.py' file
* in command line execute command 'python manage.py makemigrations'
* in command line execute command 'python manage.py migrate'
* in command line execute command 'python manage.py runserver'

## Description API ##

* http://127.0.0.1:8000/news/post/create - with 'post' method create new post 
* http://127.0.0.1:8000/news/post/all - with 'get' method return all posts
* http://127.0.0.1:8000/news/post/detail/4 - with 'put' method update post
* http://127.0.0.1:8000/news/post/detail/4 - with 'delete' method deleted post
* http://127.0.0.1:8000/news/post/detail/1 - with 'get' method return one post

* http://127.0.0.1:8000/news/post/upvode - with 'post' method add one vote to post
* http://127.0.0.1:8000/news/post/upvode - with 'put' method reset upvotes in all posts

* http://127.0.0.1:8000/news/comment/create - with 'post' method create new comment
* http://127.0.0.1:8000/news/comment/all - with 'get' method return all comments
* http://127.0.0.1:8000/news/comment/detail/1 - with 'put' method update comment
* http://127.0.0.1:8000/news/comment/detail/3 - with 'delete' method deleted comment
* http://127.0.0.1:8000/news/post/detail/1 - with 'get' method return one comment

## Postman collection ##

* https://www.getpostman.com/collections/a5bc909305aa2e4e5bf6

## Docker ##

* in command line execute command 'docker-compose build'
* in command line execute command 'docker-compose up'